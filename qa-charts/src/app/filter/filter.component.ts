import { Component} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { FilterWireServiceService } from '../services/filter-wire-service.service';
import { Subscription } from 'rxjs';
import { FilterService } from '../services/filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent {
  wireSubscription:Subscription;
  userSubscription:Subscription;
  includeTeams = false;
  membersToInclude = [];
  allMembers = [];
  filterName = '';
  UserId = 0;
  filterid = 0;
  isDefault = false;
  
  constructor(public activeModal: NgbActiveModal, private userService:UserService, 
    private filterWireService:FilterWireServiceService, private filterService:FilterService) {
      userService.callSearch('').then((data)=>{
        this.allMembers = data;
      })
      this.membersToInclude =  filterWireService.getWire();
  }

  removeUserFromContainer(userToRemove){
    var nArray = [];
    var i = 0;
    for(i=0;i<this.membersToInclude.length;i++){
      if(this.membersToInclude[i].QateID != userToRemove.QateID)
        nArray.push(Object.assign({}, this.membersToInclude[i]));
    }
    this.membersToInclude = [];
    this.membersToInclude = nArray;
  }

  QateIDfn(index, user) {
    return user.QateID;
  }

  clearUserContainer(){
    this.membersToInclude = [];
    this.includeTeams = false;
    this.filterName = '';
    this.isDefault = false;
  }

  addUserToContainer(user){
    var i=0;
    for (i; i < this.membersToInclude.length; i++) {
      if (this.membersToInclude[i].QateID == user.QateID)
          return;
    }
    this.membersToInclude.push(user);
  }

  trySearch = function() {
    if(this.filterName.length ==0){
      this.userService.callSearch('').then((data)=>{
        this.allMembers = data;
      });
    }else{
      this.userService.callSearch(this.filterName).then((data)=>{
        this.allMembers = data;
      });
    }
  };


  wireEvent(combo){
    this.membersToInclude = this.filterWireService.getWire();
    var f = this.filterWireService.getFilter();
    if (f || f.id == undefined) {
      this.filterid = 0;
      this.filterName = '';
    } else {
      this.filterid = f.id;
      //do not just set the filter id also set the include teams
      this.includeTeams = f.includeTeams;
      //also set the filter name
      this.filterName = f.filterName;
      this.isDefault = f.isDefault;
    }
  }

  userEvent(userObj){
    this.UserId = this.userService.getUserId();
  }

  getcsv(){
    var ret = '';
    //if we are including teams do not do the usernames as they will have to be prepopulateed at the servlet side otherwise use the names
    var i = 0;
    for (i; i < this.membersToInclude.length; i++) {
      ret += this.membersToInclude[i].QateID + ',';
    }
    return ret;
  }

  saveFilter(){
    //at this point we have the filter id
    //lets populate the data for our filter obj
    var filter = {
      includeTeams: this.includeTeams,
      filterName: this.filterName,
      userId: this.UserId,
      filtercsv: this.getcsv(),
      id: this.filterid,
      isDefault: this.isDefault
    };
    //at this point filter should have everything
    this.filterService.saveFilter(filter).then((data) => {
        //when we save check that the filter id is > 0
        if (data.id != undefined && data.id > 0) {
          var nArray = [];
          var i = 0;
          for (i; i < this.membersToInclude.length; i++) {
              nArray.push(Object.assign({}, this.membersToInclude[i]));
          }
          //get the wire;
          this.filterWireService.setWire(nArray, data);
          this.activeModal.close();
        }
    });
  }

  ngOnInit(){
    //Should not use event emitter
    this.wireSubscription = this.filterWireService.currentCombo$
       .subscribe(combo => this.wireEvent(combo));
    this.userSubscription = this.userService.currentUser$
      .subscribe(userObj => this.userEvent(userObj));
  }
  
  ngOnDestroy() {
    this.wireSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
  }
}
