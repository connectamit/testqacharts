import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HandleError } from '../services/service-helper';
import {MAUTH} from '../../assets/mock-auth-guard-ob'

@Injectable({
  providedIn: 'root'
})
//LoginService with UserServices
export class AuthService {
  
  constructor(private http: HttpClient) { }

  wrought(val1:string,val2:string){
    var e = val1 + ":" + val2;
    //Returns base 64 encoding
    return btoa(e);
  }

  ngOnInit(){
  }

  callLogin(user){
    this.setCookie(MAUTH.cookiekey, MAUTH.cookieval,
      new Date("2019-03-24T17:38:29.548Z"));
    this.setCookie(MAUTH.cookiekey2,MAUTH.cookieval2,new Date("2019-03-13T22:28:34.898Z"));
    this.setCookie(MAUTH.cookiekey3, MAUTH.cookieval3, new Date("2019-03-13T22:28:34.898Z"));
    //return this.http.post("http://172.17.6.22:8080/QAGantt/AngularJQV/LoginInfor?v=" + user, user);
    if(user == MAUTH.ifuser)
      return Promise.resolve({"success":true, "data":"tempUser"});
    else
      return Promise.resolve({"success":false, "data":null });
  }


  private setCookie(name: string, value: string, expireDays: Date, path: string = '/') {
    let expires:string = `expires=${expireDays.toUTCString()}`;
    let cpath:string = path ? `; path=${path}` : '';
    document.cookie = `${name}=${value}; ${expires}${cpath}`;
}
}
