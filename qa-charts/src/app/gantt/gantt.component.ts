import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import {TaskService} from "../services/task.service";
import { Subscription, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

import "dhtmlx-gantt";
import { UserService } from '../services/user.service';
import { FilterService } from '../services/filter.service';
import { FilterWireServiceService } from '../services/filter-wire-service.service';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-gantt',
  templateUrl: './gantt.component.html',
  styleUrls: ['./gantt.component.css'],
  providers: [TaskService]
})
export class GanttComponent implements OnInit {
  private totalPages=0;
  private pageTotal = new BehaviorSubject(this.totalPages);
  pageTotal$ = this.pageTotal.asObservable(); 

  @ViewChild("gantt_here") ganttContainer: ElementRef;
  constructor(private taskService: TaskService, private userService:UserService,
      private filterService:FilterService, private filterWireService:FilterWireServiceService) {
  }

  snapToToday() {
    gantt.showDate(new Date());
  };

  show_chart(){
    gantt.render();
  };

  search(getstart, getend, membersToInclude, includeTeams,page,max) {
    //no loader yet
    //show_loader();
    gantt.clearAll();
    Promise.all([this.taskService.getSearch(getstart, getend, membersToInclude, includeTeams,page,max)])
    .then(([data]) => {
        gantt.parse(data);
        this.pageTotal.next(data.total);
    });
  }

  ngOnInit() {

    //sets loading tasks date format
    gantt.config.xml_date = "%Y-%m-%d";
    gantt.config.columns = [
      {name: "text", label: "Task Name", resize: true, tree: true, width: 350},
      {name: "start_date", label: "Start Date", width: 80, align: "center"},
      {name: "end_date", label: "End Date", width: 80, align: "center"}
    ];

    gantt.config.drag_move = false;//prevent moving the tasks
    gantt.config.drag_progress = false;//prevent sliding for progress
    gantt.config.drag_links = false;//prevent sliding for links
    gantt.config.drag_resize = false;//prevent sliding for resize
    gantt.config.details_on_dblclick = false;//prevent double click action to get details
    gantt.config.show_unscheduled = true;//display tasks with no dates
    gantt.config.readonly = true;
    gantt.config.scroll_on_click = false;//prevent scrolling to view the task that was selected
    gantt.config.date_grid = "%m-%d-%Y";

    gantt.templates.scale_cell_class = function(date:Date) {
      if (date.getDay() == 0 || date.getDay() == 6) {
          return "weekend";
      }
      var tempDate = date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear();
      var today = new Date();
      var todayStr = today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear();
      if (tempDate == todayStr)
          return "today"
    };

    gantt.templates.task_cell_class = function(item, date) {
      if (date.getDay() == 0 || date.getDay() == 6) {
          return "weekend";
      }
      var dateStr = date.getMonth() + '/' + date.getDate() + '/' + date.getFullYear();
      var today = new Date();
      var todayStr = today.getMonth() + '/' + today.getDate() + '/' + today.getFullYear();
      if (dateStr == todayStr)
          return "today";
    };

    gantt.templates.task_class = function(start, end, task) {
      if (task.isBlocked) {
          return 'blocker';
      } else {
          return '';
      }
    };

    gantt.templates.tooltip_text = function(start, end, task) {
      var anchor, labels;
      anchor = '<a href="' + task.query + '" target=_blank()>';
      if (task.isLeaf) {
          labels = task.labels;
      } else {
          labels = '[See-Tasks]';
      }
      var retval = "<b>Task:</b>" + anchor + labels + task.text + "</a><br/><b>Start date:</b> " +
              gantt.templates.tooltip_date_format(start) +
              "<br/><b>End date:</b> " + gantt.templates.tooltip_date_format(end);
      return retval;
    };

    gantt.init(this.ganttContainer.nativeElement);
  }
}
