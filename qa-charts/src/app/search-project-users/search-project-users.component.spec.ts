import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchProjectUsersComponent } from './search-project-users.component';

describe('SearchProjectUsersComponent', () => {
  let component: SearchProjectUsersComponent;
  let fixture: ComponentFixture<SearchProjectUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchProjectUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchProjectUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
