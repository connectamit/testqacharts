import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { SearchService } from '../services/search.service';
import { HandleError } from '../services/service-helper';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { IUsers} from '../Interfaces/IUsers'
@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
  providers: [AuthService,SearchService]
})
export class LoginPageComponent implements OnInit {
  failedLogin: boolean = false
  subEnabled = true;
  loginForm:FormGroup;
  users:IUsers
  filteredusers:IUsers


  constructor(private router: Router, private auth: AuthService, private myservice: SearchService) { }



  submitLogin(){
    this.auth.callLogin(this.auth.wrought(this.username.value, this.password.value)).then((data) =>{
      if(data && data.success){
        this.router.navigate(['/QATaskChart'])
      }else{
        this.failedLogin = true;
      }
    }).catch(HandleError);
  }

  clearError(){
    this.subEnabled = true;
    this.failedLogin = false;
  }

  validPrefix(val){
    return (val.toLowerCase()=='extron')||(val.toLowerCase()=='extron1')
  }
  validLength(val){
    return (val!=undefined)&&(val.length>0)
  }
  validSuffix(val){
    return (val.toLowerCase()=='extron.com');
  }
  emailDomainValidator(control: FormControl) { 
    let value = control.value;
    let fail = {failedEmail: value};
    if(value==undefined){
      return fail;
    }
    //perform a quick check to see if the username is valid (must be > 0) and no / or @
    if(value.length > 0 && value.indexOf('/')<0 && value.indexOf('@')<0){
        return null;
    }else{
        //check to see if which ones we have
        var prefixTest = value.indexOf('/');
        if(prefixTest==0)
            return fail;
        var suffixTest = value.indexOf('@');
        if(suffixTest==0)
            return fail;
        if(prefixTest>=1){
            var e = value.split('/');
            if(!this.validPrefix(e[0]))
                return fail;
            if(suffixTest>0){
                var f = e[1].split('@');
                if(f.length < 2 && !this.validSuffix(f[1]))
                    return fail;
                return (f[0]!=undefined)&&(f[0].length>0)?null:fail;
            }
        }
        if(suffixTest>=1){
            var e = value.split('@');
            if((e.length !=2) || (e[1].toLowerCase()!='extron.com'))
                return fail;
            return (e[0]!=undefined)&&(e[0].length>0)?null:fail;
        }
    }
    return null;   
  }

  get username() { return this.loginForm.get('username'); }

  get password() { return this.loginForm.get('password'); }



  public checkProjects(age:any):boolean {
    console.log("I am in the Check Projects function")
    var projects = ["GSNEXT"];
    return projects.includes(age);
  }
  
 

  ngOnInit() {

  /*  this.myservice.getJSON().subscribe(data => {
      console.log(data);
  });*/
  this.myservice.getUsersData().subscribe(function(udata)
  {
    this.users = udata
    console.log("This is the data")
    
    var filtering =  ["Salvador Alvarez","Suhang Desai"];
    var objtest = this.users.filter(function(obfilter)
    {
       {
         console.log(obfilter.name)
         return filtering.includes(obfilter.name)
        }
    })
    console.log(objtest)
    console.log("This is the real data")

  })
    
  function checkProjects(age) {
    console.log("I am in the Check Projects function")
    var projects = ["GSNEXT"];
    return projects.includes(age);
  }
  /*This is second search*/

 this.myservice.getUsersData().subscribe(function(data)
 {
   this.filteredusers = data
   var filtest = this.filteredusers.filter(function(dataitem)
   {
     var that = this
     return dataitem.projects.some(function(titem)
     {
      var projects = ["GSNEXT"];
      return projects.includes(titem);
     })
   })
   console.log("This is the second filter ")
   console.log(filtest)
   console.log("This is the end of the second filet")


 })


  /*Second search is here */
    
   
    this.loginForm = new FormGroup({
      'username':new FormControl('', [
        Validators.required,
        this.emailDomainValidator
      ]),
      'password':new FormControl('', [
        Validators.required
      ])
   });
  }

}
