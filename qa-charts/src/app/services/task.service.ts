import { Injectable } from '@angular/core';
import {Task} from "../models/Task";
import {HttpClient} from '@angular/common/http';
import { HandleError } from './service-helper';

import { map } from 'rxjs/operators';
import { ɵangular_packages_router_router_h } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  private taskUrl = "api/tasks";
  constructor(private http: HttpClient) {}

  formatDate(val){
    var e = new Date(val);
    var mm = '' + (e.getMonth() + 1);
    var dd = '' + (e.getDate());
    var yyyy = '' + (e.getFullYear());
    if (mm.length < 2)
        mm = '0' + mm;
    if (dd.length < 2)
        dd = '0' + dd;
    return [yyyy, mm, dd].join('-');
  };

  //Load data from server later
  count = 0; //Not sure if this is correct
  userSet: Set<string>= new Set<string>();
  // foundSet: Set<string>= new Set<string>();
  total = 0;
  async getSearch(getstart, getend, membersToInclude, includeTeams,page,maxStr): Promise<any>{
    // return this.http.get("AngularJQV/ProcessRequest?startd=" + this.formatDate(getstart) +
    //       '&endd=' + this.formatDate(getend) +
    //       '&includeTeams=' + includeTeams +
    //       '&members=' + membersToInclude + 
    //       '&startat='+((page-1)*max)+
    //       '&max='+max
    //       ).toPromise().catch(HandleError);
    membersToInclude = membersToInclude.toLowerCase();
    var membersInTeam:string;
    if(includeTeams){
      await this.http.get<any>('assets/mock-teams.json').pipe(
        map((res) => {
          return res.filter((data)=>{
            if(membersToInclude.includes(data.username.toLowerCase())){
              return data;
            }
          })
        })).toPromise().then((data)=>{
          data.forEach(element => {
            element.team.forEach(member=>{
              membersInTeam += member.QAEFullName.toLowerCase() + ",";
            })
          });
      });
    }

    this.count=0;
    this.userSet.clear();
    var max = parseInt(maxStr,10);
    var startAt = (page-1)*max;
    return this.http.get<any>('assets/mock-task-data.json').pipe(
      map((res) => {
        return {data:res.data.filter((data)=>{
          var name:string;
          if(!isNaN(data.id)){
            var index = data.parent.indexOf('_');
            name = data.parent.substring(0,index);
          }else{
            var index = data.id.indexOf('_');
            name = data.id.substring(0,index);
            name = index>=0?name:data.id;
          }
          name = name.toLowerCase().trim();
          if(name.length<=0) return;
          if("start_date" in data && new Date(data.start_date) >=new Date(getstart) && new Date(data.end_date) <= new Date(getend)){
            if(this.userSet.has(name)){
              if(data.isLeaf){
                this.count++;
              }
              return data;
            }else{
              if((!membersToInclude || membersToInclude.includes(name)) || (includeTeams && membersInTeam.includes(name))){
                //if(!this.foundSet.has(name)){
                  if(data.isLeaf){
                    this.count++;
                  }
                  //this.foundSet.add(name);
                //}
                if(this.count>=startAt && this.count < startAt + max){
                  this.userSet.add(name);
                  return data;
                }
              }
            }
            return;
          }else if(!("start_date" in data)){
            if(this.userSet.has(name)){
              return data;
            }
          }
          return;
        })
        ,total:this.count}
      })).toPromise();

  }


}
