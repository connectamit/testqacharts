import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilterWireServiceService {
  wire=[];
  filter;
  combo = {wire:this.wire,filter:this.filter};
  
  private currentCombo = new BehaviorSubject(this.combo);
  currentCombo$ = this.currentCombo.asObservable(); 

  constructor() { }

  getWire(){
    return this.wire;
  }

  getFilter(){
    return this.filter;
  }

  setWire(data, filter){
    this.wire = data;
    this.filter = filter;
    this.combo.wire = this.wire;
    this.combo.filter = this.filter;
    this.currentCombo.next(this.combo);
  }
}
