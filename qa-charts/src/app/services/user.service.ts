import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HandleError } from './service-helper';
import { BehaviorSubject } from 'rxjs';
import { UserObj } from '../models/UserObj';

import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userName ='';
  userObj:UserObj;

  cookiename = 'whoami';

  //Should not use Event Emitter
  private currentUser = new BehaviorSubject(this.userObj);
  currentUser$ = this.currentUser.asObservable();

  constructor(private http: HttpClient) { }

  setUserName(){
    //Check Utils
    //this.userName = getUserName()
  }

  setUserObj(){
    //this.userObj={"email":"aliu@extron","userName":"aliu","QateID":928,"GroupMemberID":1,"QAEFullName":"Alex Liu"};
    return this.http.get("AngularJQV/ProcessUserFilters?type=userreq&un=" + this.userName).subscribe((response:UserObj)=>{
      this.userObj = response;
      this.currentUser.next(this.userObj);
    });
  }

  getUserId(){
    if(!this.userObj) return null;
    return this.userObj.QateID;
  }

  isLoggedIn(){
    //Check Utils
    return this.loggedIn();
  }

  logout(){
    //Logout by cookies
  }

  private loggedIn(){
    var c = this.getParsedUserCookie();
    console.log(c);
    return (c!=undefined&&c.length>0);
  }

  private getParsedUserCookie(){
    return this.getCookie(this.cookiename);
  }

  private getCookie(cname){
    var name = cname + "=";
    var ca = this.getCookies().split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
  }

  private getCookies(){
    return decodeURIComponent(document.cookie);//this gets back all of the cookies refereing to the current doc
  }

  callSearch(searchv){
    return this.http.get<any>('assets/mock-user.json').pipe(
      map((res) => {
        return res.filter((data)=>{
          if(data.QAEFullName.toLowerCase().includes(searchv.toLowerCase())){
            return data;
          }
        })
      }
      )).toPromise();
    // return this.http.get("AngularJQV/ProcessUserSearch?userSearch=" + searchv).toPromise().then((response)=>{
    //   return response["data"];
    // }).catch(HandleError);
  }
}
