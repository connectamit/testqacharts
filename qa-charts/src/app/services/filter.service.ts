import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { HandleError } from './service-helper';

@Injectable({
  providedIn: 'root'
})
export class FilterService {
 
  constructor(private http: HttpClient) { }

  //When used in component must subsrcibe to trigger, even if no callback
  saveFilter(filter){
    return this.http.put("AngularJQV/ProcessUserFilters?", filter).toPromise().catch(HandleError);
  }

  deleteFilter(filter){
    return this.http.delete("AngularJQV/ProcessUserFilters?filterId=" + filter.id).toPromise().catch(HandleError);
  }

  getUserFilters(userId){
    //return Promise.resolve([{"id":84,"isDefault":false,"idcsv":"928,","filterName":"fdsa","QateID":928,"includeTeams":false}]);
    return this.http.get("http://172.17.6.22:8080/ProcessUserFilters?userId=928");
  }

  getFilterUsers(filterid){
    return Promise.resolve([{"email":"aliu@extron","userName":"aliu","QateID":928,"GroupMemberID":1,"QAEFullName":"Alex Liu"}]);
    return this.http.get("AngularJQV/ProcessUserFilters?type=usersinfilter&filterid=" + filterid).toPromise().catch(HandleError);
  }
}
