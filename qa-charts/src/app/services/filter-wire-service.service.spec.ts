import { TestBed } from '@angular/core/testing';

import { FilterWireServiceService } from './filter-wire-service.service';

describe('FilterWireServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilterWireServiceService = TestBed.get(FilterWireServiceService);
    expect(service).toBeTruthy();
  });
});
