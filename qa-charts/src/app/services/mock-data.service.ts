import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, filter, concatMap } from 'rxjs/operators';
import { Observable, from } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class MockDataService {

  constructor(private http: HttpClient) { }

  getSearch(getstart, getend, membersToInclude, includeTeams,page,max){
    return this.http.get('..\models\mock-task-data.json').pipe(
      map((res: Response) => res.json()),
      concatMap(arr => from(arr)),
      filter(item=>item.length<=10)
      ).subscribe(res=>{
          console.log(res);
      });
  }

}
