import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { MainPageComponent } from './main-page/main-page.component';
import { AuthGuard } from './auth/auth.guard';
import { FilterComponent } from './filter/filter.component'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { GanttComponent } from './gantt/gantt.component';
import { SearchService } from './services/search.service';
import { SearchFilterComponent } from './search-filter/search-filter.component';
import { SearchUsersComponent } from './search-users/search-users.component';
import { SearchProjectsComponent } from './search-projects/search-projects.component';
import { SearchProjectUsersComponent } from './search-project-users/search-project-users.component';
import { SearchParentProjectUsersComponent } from './search-parent-project-users/search-parent-project-users.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'QATaskChart',
    component: MainPageComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginPageComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    MainPageComponent,
    FilterComponent,
    GanttComponent,
    SearchFilterComponent,
    SearchUsersComponent,
    SearchProjectsComponent,
    SearchProjectUsersComponent,
    SearchParentProjectUsersComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [SearchService],
  bootstrap: [AppComponent],
  entryComponents:[
    FilterComponent,
    SearchFilterComponent
  ]
})
export class AppModule { }
