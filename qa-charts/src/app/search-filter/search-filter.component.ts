import { Component, OnInit} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { UserService } from '../services/user.service';
import { FilterWireServiceService } from '../services/filter-wire-service.service';
import { Subscription } from 'rxjs';
import { FilterService } from '../services/filter.service';
import { SearchService } from '../services/search.service';
import { IUsers} from '../Interfaces/IUsers'


@Component({
  selector: 'app-search-filter',
 // template: `<ul>
 // <li *ngFor="let element of realmembers">{{element.name}}</li>
//</ul>
//`,
  templateUrl: './search-filter.component.html',
  styleUrls: ['./search-filter.component.css']
})
export class SearchFilterComponent{

  wireSubscription:Subscription;
  userSubscription:Subscription;
  includeTeams = false;
  membersToInclude = [];
  realmembers = []

  filterName = '';
  UserId = 0;
  filterid = 0;
  isDefault = false;

  test = 'some test string'
  
  constructor(public activeModal: NgbActiveModal, private userService:UserService, 
    private filterWireService:FilterWireServiceService, private filterService:FilterService, private myservice:SearchService) {
      this.myservice.getUsersData().subscribe((data)=>{
        this.realmembers = data;
      });
     
      
  }

 
  ngOnInit() {

    console.log("I am in ngOnInit")
    /*  this.myservice.getJSON().subscribe(data => {
        console.log(data);
    });*/
    /*this.myservice.getUsersData().subscribe(function(udata)
    {
      this.realmembers = udata;
      
      this.users = udata
      console.log("This is the data")
      
      var filtering =  ["Salvador Alvarez","Suhang Desai"];
      var objtest = this.users.filter(function(obfilter)
      {
         {
           console.log(obfilter.name)
           return filtering.includes(obfilter.name)
          }
      })
      console.log(objtest)
      console.log("This is the real data")
      this.realmembers = objtest
      console.log("This is the object data"+this.realmembers[1].name)
     

          for (let entry of this.realmembers) {
            console.log(entry.name); // 1, "string", false
        }*/
  
    //});
  }

}
