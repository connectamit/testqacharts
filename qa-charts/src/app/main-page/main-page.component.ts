import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FilterComponent } from '../filter/filter.component';
import { SearchFilterComponent } from '../search-filter/search-filter.component';
import { GanttComponent } from '../gantt/gantt.component';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FilterService } from '../services/filter.service';
import { FilterWireServiceService } from '../services/filter-wire-service.service';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  startDate;
  endDate;
  includeTeams:boolean = false;
  searchfield="";
  hasSearchResult = false;
  userSearchReturn = [];
  membersToInclude = [];
  currentFilter;
  savedFilters:any = [];
  userId = 0;
  page = 0;
  max = 50;
  totalPages = 0;
  current_date = new Date();
  current_mm = this.current_date.getMonth() + 1;
  current_yyyy = this.current_date.getFullYear();
  next_month = new Date(this.current_yyyy, this.current_mm, 1);
  current_lastday = new Date(this.next_month.getMonth() - 1).getDate();
  userSubscription:Subscription;
  wireSubscription:Subscription;
  chartSubscription:Subscription;
  recallPressed = false;
  //membersToInclude;
  //might work
  @ViewChild(GanttComponent) ganttChild;

  constructor(private modalService: NgbModal,private userService:UserService, private router:Router,
    private filterService:FilterService, private filterWireService:FilterWireServiceService) { 
  }

  private getInitialStartDate(){
    return new Date(this.current_yyyy,this.current_mm-1, 1);
  };

  private getInitialEndDate(){
    return new Date(this.current_yyyy,this.current_mm-1, this.current_lastday);
  };

  QateIDfn(index, user) {
    return user.QateID;
  }

  filterfn(index, filter){
    return filter.id;
  }
  
  searchChart() {
    //based on a criteria 
    var csv = this.getcsv();
    if(this.page>this.totalPages)
      this.page = this.totalPages;
    if(this.page<1)
      this.page = 1;
    if(this.max<=0)
      this.max = 50;
    this.ganttChild.search(this.startDate, this.endDate, csv, this.includeTeams,this.page,this.max);
  };

  getcsv() {
    var ret = '';
    //if we are including teams do not do the usernames as they will have to be prepolluateed at the servlet side otherwise use thenames
    var i = 0;
    for (i; i < this.membersToInclude.length; i++) {
        if (!this.includeTeams)
            ret += this.membersToInclude[i].QAEFullName + ',';
        else
            ret += this.membersToInclude[i].QAEFullName + ',';
    }
    return ret;
    //CHANGE TO THIS LATER
    // for (i; i < this.membersToInclude.length; i++) {
    //     if (!this.includeTeams)
    //         ret += this.membersToInclude[i].userName + ',';
    //     else
    //         ret += this.membersToInclude[i].QateID + ',';
    // }
    // return ret;
  };

  savedFiltersModal(){
    //deep copy the objects for the wire
    var nArray = [];
    var i = 0;
    for (i; i < this.membersToInclude.length; i++) {
        nArray.push(Object.assign({}, this.membersToInclude[i]));
    }
    //get the wire;
    this.filterWireService.setWire(nArray, this.currentFilter);
  }

  recallFilter(filter) {
    if (filter.toDelete != undefined) {
        return;//do not do anything
    }
    //set the values of the filter based on this filter
    //first clear the values being displayed.
    this.clearUserContainer();
    this.currentFilter = filter;//store a reference to it
    //next set the include teams
    this.includeTeams = filter.includeTeams;
    //next set the user container using the filter services
    this.filterService.getFilterUsers(filter.id).then((data)=>{
        //when this comes back it will have all of the users so set the value
        this.membersToInclude = data;
        this.searchChart();
    });
  };

  deleteFilter(filter) {
    filter.toDelete = true;
    this.filterService.deleteFilter(filter).then((data)=>{
        //this should contain a delete scucess or true
        if (!data.success) {
            alert('Failed to delete');
        }
    });
    //delete this filter from the list
    var i = 0;
    var nArray = [];
    for (i; i < this.savedFilters.length; i++) {
        if (this.savedFilters[i].id != filter.id) {
            nArray.push(Object.assign({}, this.savedFilters[i]));
        }
    }
    this.savedFilters = [];
    this.savedFilters = nArray;
  };
  
  getDefaultFilter = function() {
    if (this.savedFilters.length > 0) {
        //we have more than 1 filter so try it 
        var i = 0;
        for (i; i < this.savedFilters.length; i++) {
            if (this.savedFilters[i].isDefault) {
                return this.savedFilters[i];
            }
        }
        return {};//if we did not FIND the filter then return 
    } else {
        return {};//return an empty object as there are no filters
    }
  };
  
  addToUserContainer(user){
    var i=0;
    for (i; i < this.membersToInclude.length; i++) {
      if (this.membersToInclude[i].QateID == user.QateID)
          return;
    }
    this.membersToInclude.push(user);
    this.removeUsers();
    this.searchfield = '';
  }

  removeUserFromContainer(userToRemove){
    var nArray = [];
    var i = 0;
    for(i=0;i<this.membersToInclude.length;i++){
      if(this.membersToInclude[i].QateID != userToRemove.QateID)
        nArray.push(Object.assign({}, this.membersToInclude[i]));
    }
    this.membersToInclude = [];
    this.membersToInclude = nArray;
  }

  logOut() {
    //delete the cookies and redirect
    this.userService.logout();
    this.router.navigate(['/login']);
  };

  removeUsers(){
    this.userSearchReturn = [];
    this.hasSearchResult = false;
  }

    //TODO
  checkDropdowns(){

  }

  //TODO
  displayFiltersDropDown(){
    if (this.savedFilters.length > 0) {
      var elm = document.getElementById("recallFitersDropDown");
      elm.className += ' show';
    }
  }

  trySearch = function() {
    //if search field is > 3 try the search or if it equals ' ' then try
    if (this.searchfield.length >= 3) {
        //call the search
        this.userService.callSearch(this.searchfield).then((data)=>{
          this.userSearchReturn = data;
            //display the results and cause a show
            if (this.userSearchReturn.length > 0)
              this.hasSearchResult = true;
            else
              this.hasSearchResult = false;
        });
    }
    if (this.searchfield.length == 0) {
      this.userSearchReturn = [];
      this.hasSearchResult = false;
    }
  };

  spacePress($event){
    if ($event.keyCode === 32 || $event.charCode === 32) {
      this.userService.callSearch('').then((data)=>{
          this.userSearchReturn = data;
          if (this.userSearchReturn.length > 0)
              this.hasSearchResult = true;
          else
              this.hasSearchResult = false;
      });
    }
  }
  
  clearUserContainer(){
    this.membersToInclude = [];
    this.includeTeams = false;
    this.currentFilter = {};//zero out this filter as well
    this.searchfield = '';
  }

  openFilterModal() {
    const modalRef = this.modalService.open(FilterComponent, { size: 'lg'});
    //modalRef.componentInstance.name = 'saveFilter'; can set values in modal
  }

  openSearchFilterModal() {

    const modalRef = this.modalService.open(SearchFilterComponent);
  
    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
    console.log("I am in the OpenSearchFilter")
    //modalRef.componentInstance.name = 'saveFilter'; can set values in modal
  }

  getChartFirstPage(){
    this.page = 1;
    this.searchChart();
  };

  getChartPreviousPage(){
    this.page -=(this.page-1>=1)?1:0;
    this.searchChart();
  };

  getChartNextPage(){
    this.page +=(this.page+1<=this.totalPages)?1:0;
    this.searchChart();
  };

  getChartLastPage(){
    this.page =this.totalPages;
    this.searchChart();
  };

  goToPage(){
    if(!this.page) return;
    if(this.page > this.totalPages){
      this.page = this.totalPages;
    }else if(this.page <1){
      this.page = 1;
    }
    this.searchChart();
  }

  userEvent(userObj){
    this.userId = this.userService.getUserId();
    this.filterService.getUserFilters(this.userId).subscribe((data) => {
        this.savedFilters = data;
        //check to see if we have a valid default filter
        this.currentFilter = this.getDefaultFilter();
        if (this.currentFilter != {} && this.currentFilter.id != undefined) {
            //if we are not empty then do the search
            this.recallFilter(this.currentFilter);
        } else {
            //hide the loading chart
            // ChartServices.hide_loader();
        }
      });
  }

  wireEvent(combo){
    this.membersToInclude = this.filterWireService.getWire();
    var f = this.filterWireService.getFilter();
    //because this was set from the save filter then we should make THIS
    //the current filter
    if(f){
      this.currentFilter = f;
      this.includeTeams = f.includeTeams;
      //anytime the filters change perform a reload of the filters
      this.filterService.getUserFilters(this.userId).subscribe((data)=>{
        this.savedFilters = data;
      });
    }
  }

  pageEvent(totalPages){
    this.totalPages = Math.ceil(totalPages/this.max);
    if(this.totalPages != 0 && this.page > this.totalPages){
      this.page = this.totalPages;
      this.searchChart();
    }
  }

  ngOnInit() {
    this.userSubscription = this.userService.currentUser$
      .subscribe(userObj => this.userEvent(userObj));
    this.wireSubscription = this.filterWireService.currentCombo$
      .subscribe(combo => this.wireEvent(combo));
    this.chartSubscription = this.ganttChild.pageTotal$
      .subscribe(pageTotal => this.pageEvent(pageTotal));
  }

  ngAfterContentInit() {
    this.startDate = this.getInitialStartDate().toISOString().split('T')[0];
    this.endDate = this.getInitialEndDate().toISOString().split('T')[0];
  }

  ngOnDestroy() {
    this.wireSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.chartSubscription.unsubscribe();
  }

}
