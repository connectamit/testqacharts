import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchParentProjectUsersComponent } from './search-parent-project-users.component';

describe('SearchParentProjectUsersComponent', () => {
  let component: SearchParentProjectUsersComponent;
  let fixture: ComponentFixture<SearchParentProjectUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchParentProjectUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchParentProjectUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
