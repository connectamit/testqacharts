
export class returnData{
    id: string;
    start_date: string;
    text: string;
    parent: string;
    color: string;
    end_date: string;
    isBlocked: boolean;
    isLeaf: boolean;
    key: string;
    labels: string;
    query: string;
    tstate: number;
}

export class Task {
    id: string;
    start_date: string;
    text: string;
    parent: string;
    color: string;
    end_date: string;
    isBlocked: boolean;
    isLeaf: boolean;
    key: string;
    labels: string;
    query: string;
    tstate: number;
}