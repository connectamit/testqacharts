import { browser, by, element } from 'protractor';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitleText() {
    return element(by.xpath('//title')).getText();
  }
  getLoginHeader(){
    return element(by.xpath('//h3')).getText();
  }
  getLoginFailedMessage(){
    return element(by.id('loginFailedBanner')).getText();
  }
  submit(){
    return element(by.xpath('//button')).click();
  }
  getSubmitText(){
    return element(by.xpath('//button')).getText();
  }
  setUserName(){
    return element(by.xpath('(//input)[1]')).sendKeys('sal');
  }
  setPassword(){
    return element(by.xpath('(//input)[2]')).sendKeys('sdfasdafs');
  }
}
