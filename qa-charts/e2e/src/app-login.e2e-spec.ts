import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('Login page test',async () => {
  let page: AppPage;
  

  beforeEach(async () => {
    page = new AppPage();
    await page.navigateTo();
    await browser.sleep(15000);
  });

  xit('should display welcome message',async () => {
    await page.getTitleText().then(text=>{
      expect(text).toEqual('');
    });
  });

  xit('should display Login',async()=>{
    await page.getLoginHeader().then(text=>{
      expect(text).toEqual('Login');
    })
  });

  it('should display a failed to login message if the password is incorrect',async()=>{
    await page.setUserName();
    await page.setPassword();
    await page.submit();
    await page.getSubmitText().then(text=>{
      expect(text).toEqual('Submit');
    })
    await page.getLoginFailedMessage().then(text=>{
      expect(text).toEqual('Failed to Login');
    });
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    }));
  });
});
